<?php
/*
 * Template Name: Services
 * Description: Template for the landing page.
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$query = array(
 'post_type' => 'service',
 'orderby' => 'date',
 'order' => 'ASC'
);
query_posts($query);
$context['services'] = Timber::get_posts();
Timber::render( array( 'page-services.twig', 'page.twig' ), $context );