<?php
/*
 * Template Name: Team
 * Description: Template for the landing page.
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$query = array(
 'post_type' => 'person',
 'orderby' => 'date',
 'order' => 'ASC'
);
query_posts($query);
$context['team'] = Timber::get_posts();
Timber::render( array( 'page-team.twig', 'page.twig' ), $context );